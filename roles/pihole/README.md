# Additional setup steps

- Make sure to add a local DNS entry to pihole for `pihole.local` to get this to cooperate with traefik.

# Potential Issues

If pihole fails to start since "port 53 is already in use", disable and stop `systemd-resolved.service` and add a known DNS server to `/etc/resolv.conf` like so:

```bash
# Comment out previous nameserver
# nameserver 127.0.0.53
nameserver 8.8.8.8 # Google's DNS Server
```

This should be fixed with copying the `resolved.conf` and restarting the service. I'm using Quad9 as a primary DNS provider and Google as a secondary, but feel free to change it to whatever suits you. This role is largely intended for setting up a DNS server from scratch, so if you're doing shenanigans with resolved you probably want to roll your own here instead.
