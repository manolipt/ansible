# Notes

- Requires manual setup steps through Nextcloud AIO Interface
- If getting "502 bad gateway" from traefik, try changing `server_ip` to your server IP on your local network.
- OIDC Setup Guide: https://blog.cubieserver.de/2022/complete-guide-to-nextcloud-oidc-authentication-with-authentik/