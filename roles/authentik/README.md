# Notes

- Make sure to set password on your non-default user before logging out as admin
- Setup URL is `https://{your-authentik-domain}/if/flow/initial-setup`
