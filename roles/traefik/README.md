# Notes

DDNS or Static IP required for this to be production-stable. Currently running mine through my TP-Link router, but you can run this for free through DuckDNS or roll your own container/script with your registrar's API.
